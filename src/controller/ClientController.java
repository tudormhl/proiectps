package controller;

import java.util.Observable;
import java.util.Observer;

import model.Admin;
import model.Waiter;
import model.KitchenEmployee;
import client.Client;

public class ClientController implements Observer {

	Client client;
	public ClientController(Client client) {
		this.client = client;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if ( arg1 instanceof Waiter ) {
			if (((Waiter) arg1).getOperation().equals("checkLogin")){	
				this.client.sendToServer(arg1);
			}	
			
			if (((Waiter) arg1).getOperation().equals("showProducts")){	
				this.client.sendToServer(arg1);
				System.out.println("ClientController");
			}
			
			if (((Waiter) arg1).getOperation().equals("makeOrder")){
				this.client.sendToServer(arg1);
			}
			
			if (((Waiter) arg1).getOperation().equals("deleteOrder")){
				this.client.sendToServer(arg1);
			}
			
			if (((Waiter) arg1).getOperation().equals("goBack")){
				this.client.sendToServer(arg1);
			}

		}
		if ( arg1 instanceof Admin ) {
			if (((Admin) arg1).getOperation().equals("checkLogin")){	
				this.client.sendToServer(arg1);
			}	
			if (((Admin) arg1).getOperation().equals("seeWaiterActivity")){	
				this.client.sendToServer(arg1);
			}
			if (((Admin) arg1).getOperation().equals("seeActivity")){	
				this.client.sendToServer(arg1);
			}
		}
		if ( arg1 instanceof KitchenEmployee ) {
			if (((KitchenEmployee) arg1).getOperation().equals("checkLogin")){	
				this.client.sendToServer(arg1);
			}
		}
		
	}

}
