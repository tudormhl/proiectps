package controller;

import dal.UserOperation;

public class LoginController {
	
	private UserOperation userOp;
	
	public LoginController() {
		this.userOp = new UserOperation();
	}
	
	public boolean login( String type, String username, String password){
		Boolean valid = false;
		if(!userOp.findByNameAndPassword(type, username, password)== true){
            valid = true;
		}
        return valid;
    }
}
