package controller;

import java.util.List;

import dal.UserOperation;
import model.Order;

public class WaiterController {

	private UserOperation uo;
	
	public WaiterController(){
		uo = new UserOperation();
	}
	
	public List<String> getProducts(String products){
		return this.uo.getProducts(products);
	}
	
	public List<String> getDrinks(String products){
		return this.uo.getDrinks(products);
	}
	
	public List<String> getDeserts(String products){
		return this.uo.getDeserts(products);
	}
	
	public void insertOrder(Order order){
		this.uo.addOrder(order);
	}
	
	public int getProductId(String nume){
		return this.uo.getProductId(nume);
	}
	
	public int getDrinkId(String nume){
		return this.uo.getDrinkId(nume);
	}
	
	public int getDesertId(String nume){
		return this.uo.getDesertId(nume);
	}
	
	public int getWaiterId(String nume){
		return this.uo.getWaiterId(nume);
	}
	
	public void updateProdQuant(int quantity,String nume){
		this.uo.updateQuantityProdus(quantity, nume);
	}
	
	public void updateDrinkQuant(int quantity,String nume){
		this.uo.updateQuantityDrink(quantity, nume);
	}
	
	public void updateDesertQuant(int quantity,String nume){
		this.uo.updateQuantityDesert(quantity, nume);
	}
	
	public void deleteOrder(int id){
		this.uo.delete(id);
	}
}
