package controller;

import java.util.List;

import dal.UserOperation;
import model.Order;

public class AdminController {
	
private UserOperation uo;
	
	public AdminController(){
		uo = new UserOperation();
	}
	
	public List<String> getWaiters(){
		return uo.getWaiter();
	}

	public int getIdWaiterByName(String username) {
		return uo.getWaiterId(username);
	}
	
	public List<Order> getActivityWaiter(int idWaiter){
		return this.uo.getOrders(idWaiter);
	}

}
