package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

public class ConnectionBridge extends Thread {
	
	private Server server;
	private Socket clientSocket;
	
	private ObjectInputStream input;
	private ObjectOutputStream output;
	
	private boolean readyToStop;
	
	protected ConnectionBridge(ThreadGroup group, Socket clientSocket,Server server) throws IOException {
		
		super(group,(Runnable)null);
		
		this.clientSocket = clientSocket;
		clientSocket.setSoTimeout(0); // make sure timeout is infinite
		
		this.server = server;

		try{
			input = new ObjectInputStream(clientSocket.getInputStream());
			output = new ObjectOutputStream(clientSocket.getOutputStream());
		}catch (IOException ex){
			try{
				closeAll();
			}catch (Exception exc) { 	
			}
			throw ex;// Rethrow the exception.
		}	
		readyToStop = false;
		start(); // Start the thread waits for data from the socket
	}

	public void sendToClient(Object msg){
		if (clientSocket == null || output == null)
			try {
				throw new SocketException("Socket does not exist");
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    try {
			output.writeObject(msg);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	    }
   }

   final public void close() throws IOException{
	   readyToStop = true; // Set the flag that tells the thread to stop
	   closeAll();
   }

   final public InetAddress getInetAddress(){
		return clientSocket == null ? null : clientSocket.getInetAddress();
   }

   public String toString(){
		return clientSocket == null ? null : clientSocket.getInetAddress().getHostName()+" (" + clientSocket.getInetAddress().getHostAddress() + ")";
   }

   final public void run(){
		server.clientConnected(this);
		try{
			Object msg;
			while (!readyToStop){
				try { 
					msg = input.readObject();
					if (handleMessageFromClient(msg)){						
						server.handleMessageFromClient(msg, this);
					}
				} 
				catch(Exception e){
					System.out.println(e);
				}
			}
	  }catch (Exception exception){
			exception.printStackTrace();
			if (!readyToStop){
				try{
					closeAll();
				}catch (Exception ex){ 
					ex.printStackTrace();
				}	
			}			
	  }finally{	 
	     server.clientDisconnected(this); 
	  }
    }

	private boolean handleMessageFromClient(Object message){
		return true;
	}

	final private void closeAll() throws IOException{
		try{
			if (clientSocket != null)
				clientSocket.close();
			if (output != null)
			    output.close();
			if (input != null)
				input.close();
	    }finally{
			output = null;
			input = null;
			clientSocket = null;
		}
    }
	
	protected void finalize(){
		try{
			closeAll();
		}catch(IOException e) {	
		}		
	}
	
	public Socket getSocket(){
		return this.clientSocket;
	}
}
