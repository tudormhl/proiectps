package server;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import controller.AdminController;
import controller.LoginController;
import controller.WaiterController;
import model.Admin;
import model.Waiter;
import model.KitchenEmployee;
import model.Order;

public class Server extends Thread{

	private ServerSocket serverSocket;
	private ThreadGroup clientThreadGroup;
	private int serverPort;
	public LoginController controllerLogin;
	private WaiterController waiterController;
	private AdminController adminController;
	
	public Server(int port){
		this.serverPort = port;
		this.clientThreadGroup = new ThreadGroup("ConnectionToClient threads");
		try {
			this.serverSocket = new ServerSocket(this.serverPort);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		while(true){
			Socket clientSocket = null;
			try {
				clientSocket = this.serverSocket.accept();
			}catch (IOException e) {
				e.printStackTrace();
			}
			synchronized(this){
				try {
					new ConnectionBridge( this.clientThreadGroup, clientSocket, this);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}	
	}
	
	public void clientConnected(ConnectionBridge client) {
		System.out.println("\nClient connected host: "+client.getSocket().getLocalAddress()+
						   " at port: "+client.getSocket().getLocalPort()+"\n");	
	}

	public void handleMessageFromClient(Object msg, ConnectionBridge client) {
		if(msg instanceof String)	{
			if( msg.equals("createControllerLogin")){
				this.controllerLogin = new LoginController();
			}
		}
		if ( msg instanceof Waiter ){
			if( ((Waiter) msg).getOperation().equals("checkLogin") ){
				if ( controllerLogin.login("Waiter", ((Waiter) msg).getUsername(), ((Waiter) msg).getPassword())){
					((Waiter) msg).setOperation("loginSuccesWaiter");
					client.sendToClient((Waiter) msg);
				}	
			}
			
			if(((Waiter) msg).getOperation().equals("showProducts")){
				this.waiterController = new WaiterController();
				System.out.println("asdServer");
				List<String> products = this.waiterController.getProducts("Product");
				List<String> drinks = this.waiterController.getDrinks("Drink");
				List<String> deserts = this.waiterController.getDeserts("Desert");
				if(products.isEmpty() == false){
					((Waiter) msg).setRequestedInfo(products);
					((Waiter) msg).setRequestedInfo1(drinks);
					((Waiter) msg).setRequestedInfo2(deserts);
					client.sendToClient(((Waiter) msg));
				}
			}
			
			if(((Waiter) msg).getOperation().equals("makeOrder")){
				this.waiterController = new WaiterController();
				@SuppressWarnings("unchecked")
				ArrayList<Integer> numere = (ArrayList<Integer>)((Waiter) msg).getRequestedInfo();
				@SuppressWarnings("unchecked")
				ArrayList<String> string = (ArrayList<String>)((Waiter) msg).getRequestedInfo1();
				System.out.println(string.toString());
				Order order = new Order();
				order.setProductId(waiterController.getProductId(string.get(0)));
				order.setDrinkId(waiterController.getDrinkId(string.get(1)));
				order.setDesertId(waiterController.getDesertId(string.get(2)));
				order.setWaiterId(waiterController.getWaiterId(((Waiter) msg).getUsername()));
				order.setTableNr(numere.get(3));
				
				waiterController.insertOrder(order);
				waiterController.updateProdQuant(numere.get(0), string.get(0));
				waiterController.updateDrinkQuant(numere.get(1), string.get(1));
				waiterController.updateDesertQuant(numere.get(2), string.get(2));
				((Waiter) msg).setOperation("succesInsertOrder");
				client.sendToClient((Waiter) msg);
			
			}
			
			if(((Waiter) msg).getOperation().equals("deleteOrder")){
				this.waiterController = new WaiterController();
				int id = (int) ((Waiter) msg).getRequestedInfo();
				waiterController.deleteOrder(id);
				
				((Waiter) msg).setOperation("succesDeleteOrder");
				client.sendToClient((Waiter) msg);
				
			}
			
			if(((Waiter) msg).getOperation().equals("goBack")){
				((Waiter) msg).setOperation("succesGoBack");
				client.sendToClient((Waiter) msg);
			}
			
		}
		if ( msg instanceof Admin ){
			if( ((Admin) msg).getOperation().equals("checkLogin") ){
				if ( controllerLogin.login("Admin", ((Admin) msg).getUsername(), ((Admin) msg).getPassword())){
				    ((Admin) msg).setOperation("loginSuccesAdmin");
					client.sendToClient((Admin) msg);
				}	
			}
			if (((Admin) msg).getOperation().equals("seeWaiterActivity")){	
				this.adminController = new AdminController();
				List<String> waiters = this.adminController.getWaiters();
				int i =1;
				for(String s: waiters){
					s = s+i;
					i++;
				}
				((Admin) msg).setRequestedInfo(waiters);
				client.sendToClient((Admin) msg);
			}
			if (((Admin) msg).getOperation().equals("seeActivity")){	
				this.adminController = new AdminController();
				String username = (String) ((Admin) msg).getRequestedInfo();
				int id = this.adminController.getIdWaiterByName(username);
				List<Order> orders = this.adminController.getActivityWaiter(id);
				((Admin) msg).setRequestedInfo(orders);
				client.sendToClient((Admin) msg);
			}
		}
		
		if ( msg instanceof KitchenEmployee ){
			if( ((KitchenEmployee) msg).getOperation().equals("checkLogin") ){
				if ( controllerLogin.login("Teacher",((KitchenEmployee) msg).getUsername(), ((KitchenEmployee) msg).getPassword())){
					((KitchenEmployee) msg).setOperation("loginSuccesKitchenEmployee");
					client.sendToClient((KitchenEmployee) msg);
				}	
			}
		}
		
		
	}

	public void clientDisconnected(ConnectionBridge client) {
		System.out.println("\nClient connected host: "+client.getSocket().getLocalAddress()+
				   " at port: "+client.getSocket().getLocalPort()+"\n");
	}
}
