package model;

import java.io.Serializable;
import java.util.Comparator;

public class Waiter implements Serializable ,  Comparator<Waiter>{

	public Object getRequestedInfo1() {
		return requestedInfo1;
	}
	public void setRequestedInfo1(Object requestedInfo1) {
		this.requestedInfo1 = requestedInfo1;
	}
	public Object getRequestedInfo2() {
		return requestedInfo2;
	}
	public void setRequestedInfo2(Object requestedInfo2) {
		this.requestedInfo2 = requestedInfo2;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String username;
	private String password;
	private String operation;
	private Object requestedInfo;
	private Object requestedInfo1;
	private Object requestedInfo2;
	
	public Waiter(){
	}
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Object getRequestedInfo() {
		return requestedInfo;
	}
	public void setRequestedInfo(Object requestedInfo) {
		this.requestedInfo = requestedInfo;
	}
	@Override
	public int compare(Waiter arg0, Waiter arg1) {
		// TODO Auto-generated method stub
		return arg0.getId() - arg1.getId();
	}
	
	
}
