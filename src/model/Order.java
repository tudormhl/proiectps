package model;

import java.io.Serializable;


public class Order implements Serializable {
	
	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getDrinkId() {
		return drinkId;
	}

	public void setDrinkId(int drinkId) {
		this.drinkId = drinkId;
	}

	public int getDesertId() {
		return desertId;
	}

	public void setDesertId(int desertId) {
		this.desertId = desertId;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Object getRequestedInfo() {
		return requestedInfo;
	}

	public void setRequestedInfo(Object requestedInfo) {
		this.requestedInfo = requestedInfo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWaiterId() {
		return waiterId;
	}

	public void setWaiterId(int waiterId) {
		this.waiterId = waiterId;
	}

	public int getTableNr() {
		return tableNr;
	}

	public void setTableNr(int tableNr) {
		this.tableNr = tableNr;
	}

	private int id;
	private int productId;
	private int drinkId;
	private int desertId;
	private int waiterId;
	private int tableNr;
	private String operation;
	private Object requestedInfo;
	
	public Order(){
		
	}
	
	public String toString(){
		return id+"|"+productId+"|"+drinkId+"|"+desertId+"|"+waiterId+"|"+tableNr;
	}
		

}
