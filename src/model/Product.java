package model;

import java.io.Serializable;


public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Object getRequestedInfo() {
		return requestedInfo;
	}

	public void setRequestedInfo(Object requestedInfo) {
		this.requestedInfo = requestedInfo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	private int id;
	private int orderId;
	private String name;
	private int quantity;
	private String operation;
	private Object requestedInfo;
	
	public Product(){
		
	}


}


