package model;

import java.io.Serializable;

public class Admin implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String username;
	private String password;
	private String operation;
	private Object requestedInfo;
	
	public Admin(){
	}
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Object getRequestedInfo() {
		return requestedInfo;
	}
	public void setRequestedInfo(Object requestedInfo) {
		this.requestedInfo = requestedInfo;
	}
	
}
