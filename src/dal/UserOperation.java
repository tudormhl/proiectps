package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Order;

public class UserOperation {
	
	private Connection conn;
	private String sql ;
    private PreparedStatement ps;
    private ResultSet rs;
    private DBConnection dbc;
	
	public UserOperation(){
		this.dbc = new DBConnection();
	}
	
	//----------------------------------------------- FIND 
	public boolean findByNameAndPassword(String table, String username , String password){
		boolean valid = false;
    	this.sql ="select * from " + table + " where username = ? and password = ?;";
    	try {
    		conn = dbc.connect();
			this.ps = this.conn.prepareStatement(sql);
			this.ps.setString(1,username);
			this.ps.setString(2,password);
			this.rs = ps.executeQuery();
			if(!rs.first()){
				 valid = true;
				}
			//conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return valid;
    }
	
	public List<String> getProducts(String table){
		List<String> products = new ArrayList<String>();
		this.sql = "select name from " + table;
		try{
			conn = dbc.connect();
			this.ps = this.conn.prepareStatement(sql);
			this.rs = ps.executeQuery();
			while(rs.next()){
				products.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return products;
	}
	
	public List<String> getDrinks(String table){
		List<String> products = new ArrayList<String>();
		this.sql = "select name from " + table;
		try{
			conn = dbc.connect();
			this.ps = this.conn.prepareStatement(sql);
			this.rs = ps.executeQuery();
			while(rs.next()){
				products.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return products;
	}
	
	public List<String> getDeserts(String table){
		List<String> products = new ArrayList<String>();
		this.sql = "select name from " + table;
		try{
			conn = dbc.connect();
			this.ps = this.conn.prepareStatement(sql);
			this.rs = ps.executeQuery();
			while(rs.next()){
				products.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return products;
	}
	
	public int getProductId(String nume){
		this.sql = "select id from Product where name = ?";
		int id=1;
		try{
			conn = dbc.connect();
			this.ps = this.conn.prepareStatement(sql);
			ps.setString(1, nume);
			this.rs = ps.executeQuery();
			if(rs.next())
			{
				id = Integer.parseInt(rs.getString("id"));
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}	
		return id;
	}
	
	public int getDrinkId(String nume){
		this.sql = "select id from Drink where name = ?";
		int id=1;
		try{
			conn = dbc.connect();
			this.ps = this.conn.prepareStatement(sql);
			ps.setString(1, nume);
			this.rs = ps.executeQuery();
			if(rs.next())
			{
				id = Integer.parseInt(rs.getString("id"));
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}	
		return id;
	}
	
	public int getDesertId(String nume){
		this.sql = "select id from Desert where name = ?";
		int id=1;
		try{
			conn = dbc.connect();
			this.ps = this.conn.prepareStatement(sql);
			ps.setString(1, nume);
			this.rs = ps.executeQuery();
			if(rs.next())
			{
				id = Integer.parseInt(rs.getString("id"));
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}	
		return id;
	}
	
	public int getWaiterId(String nume){
		this.sql = "select id from Waiter where username = ?";
		int id=1;
		try{
			conn = dbc.connect();
			this.ps = this.conn.prepareStatement(sql);
			ps.setString(1, nume);
			this.rs = ps.executeQuery();
			if(rs.next())
			{
				id = Integer.parseInt(rs.getString("id"));
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}	
		return id;
	}
	
	public void addOrder(Order order) {	
		System.out.println(order.getWaiterId());
		
			this.sql = "INSERT INTO `order` ( tableNr , idProdus , idWaiter , idDrink , idDesert) values (?,?,?,?,?);";
			try{
				conn = dbc.connect();
				ps = conn.prepareStatement(sql);
				ps.setInt(1, order.getTableNr());
				ps.setInt(2, order.getProductId());
				ps.setInt(3, order.getWaiterId());
				ps.setInt(4, order.getDrinkId());
				ps.setInt(5, order.getDesertId());
				
				this.ps.executeUpdate();

			}catch (SQLException e)
			{
				e.printStackTrace();
			
			}
		}
	
	public void updateQuantityProdus(int quantity, String nume){
		this.sql = "update product set quantity = ? where name = ?;";
		try{
			conn = dbc.connect();
			this.ps =  this.conn.prepareStatement(sql);
			this.ps.setInt(1, quantity);
			this.ps.setString(2, nume+"");
			this.ps.executeUpdate();
			conn.close();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void updateQuantityDrink(int quantity, String nume){
		this.sql = "update drink set quantity = ? where name = ?;";
		try{
			conn = dbc.connect();
			this.ps =  this.conn.prepareStatement(sql);
			this.ps.setInt(1, quantity);
			this.ps.setString(2, nume+"");
			this.ps.executeUpdate();
			conn.close();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void updateQuantityDesert(int quantity, String nume){
		this.sql = "update desert set quantity = ? where name = ?;";
		try{
			conn = dbc.connect();
			this.ps =  this.conn.prepareStatement(sql);
			this.ps.setInt(1, quantity);
			this.ps.setString(2, nume+"");
			this.ps.executeUpdate();
			conn.close();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	public List<String> getWaiter(){
		List<String> products = new ArrayList<String>();
		this.sql = "select username from waiter;";
		try{
			conn = dbc.connect();
			this.ps = this.conn.prepareStatement(sql);
			this.rs = ps.executeQuery();
			while(rs.next()){
				products.add(rs.getString("username"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		return products;
	}
	
	public List<Order> getOrders(int idWaiter){
		List<Order> orders = new ArrayList<Order>();
		this.sql = "select id,tableNr,idProdus,idDrink,idDesert "
				+ "from `order` "
				+ "where idWaiter =?;";
		try{
			conn = dbc.connect();
			this.ps = this.conn.prepareStatement(sql);
			this.ps.setInt(1, idWaiter);
			this.rs = ps.executeQuery();
			while(rs.next()){
				Order o = new Order();
				o.setId(rs.getInt("id"));
				o.setDesertId(rs.getInt("idDesert"));
				o.setDrinkId(rs.getInt("idDrink"));
				o.setProductId(rs.getInt("idProdus"));
				o.setTableNr(rs.getInt("tableNr"));
				o.setWaiterId(idWaiter);
				orders.add(o);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orders;
	}
	
	public void delete (int id)
	{
		this.sql = "delete from `order` where id = ?;";
		
		try{
			conn = dbc.connect();
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			this.ps.executeUpdate();
			conn.close();
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	

		
	
	
	
}
