package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.Admin;
import model.Waiter;
import model.KitchenEmployee;

public class LoginView extends Observable implements ActionListener {

	 private final JTextField usernameTextField;
	 private final JTextField passwordTextField;
	 private final JLabel userL;
	 private final JLabel passL;
	 private final JButton loginButton;
	 private JFrame frame;
	 private JPanel contentPane;
	 private JComboBox<String> comboBox;
	 
	 public LoginView() {
		  this.frame = new JFrame();
		  this.contentPane = new JPanel();
		  
		  this.frame.setBounds(100, 100, 645, 379);
		  this.frame.setBackground(Color.BLUE);
		  this.frame.setVisible(true);
		  this.frame.setResizable(false);

		  this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		  this.contentPane.setLayout(null);
		  
		  
		  this.userL = new JLabel("Username :");
		  this.userL.setBounds(100, 100, 120, 30);
		  this.usernameTextField = new JTextField();
		  this.usernameTextField.setBounds(250, 100, 120, 30);
		  this.passL = new JLabel("Password :");
		  this.passL.setBounds(100, 150, 120, 30);
		  this.passwordTextField = new JTextField();
		  this.passwordTextField.setBounds(250, 150, 120, 30);
		  this.loginButton = new JButton("Login");
		  this.loginButton.setBounds(100, 250, 120, 30); 
		  
		  String data[]= {"Admin","Waiter","KitchenEmployee"};
		  this.comboBox = new JComboBox<String>(data);
		  this.comboBox.setBounds(100, 200, 120, 30);
		    
		   this.comboBox.setSelectedIndex(2);
		  
		  this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  this.frame.setMinimumSize(new Dimension(200, 200));
		  this.frame.setLayout(new BoxLayout(this.getFrame().getContentPane(), BoxLayout.PAGE_AXIS));

	      // add to frame
		  this.contentPane.add(this.userL);
		  this.contentPane.add(this.usernameTextField);
		  this.contentPane.add(this.passL);
		  this.contentPane.add(this.passwordTextField);
		  this.contentPane.add(this.loginButton);
		  this.contentPane.add(this.loginButton);
		  this.contentPane.add(this.comboBox);
		  
		  this.frame.add(this.contentPane);
	      
	      //action lister
	      this.loginButton.addActionListener(this); 
	 }
	 
	 public String getUsername() {
	      return usernameTextField.getText();
	 }
	 
	 public String getPassword() {
	      return passwordTextField.getText();
	 }

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if ( e.getSource() == this.loginButton ){
			String userType = this.comboBox.getSelectedItem().toString();
			if( userType.equals("Waiter")){
				Waiter user = new Waiter();
				user.setUsername(this.usernameTextField.getText());
				user.setPassword(this.passwordTextField.getText());
				user.setOperation("checkLogin");	
				setChanged();
				notifyObservers(user);
			}
			if( userType.equals("Admin")){
				Admin user = new Admin();
				user.setUsername(this.usernameTextField.getText());
				user.setPassword(this.passwordTextField.getText());
				user.setOperation("checkLogin");	
				setChanged();
				notifyObservers(user);	
			}
			if( userType.equals("KitchenEmployee")){
				KitchenEmployee user = new KitchenEmployee();
				user.setUsername(this.usernameTextField.getText());
				user.setPassword(this.passwordTextField.getText());
				user.setOperation("checkLogin");	
				setChanged();
				notifyObservers(user);
			}
			this.frame.setVisible(false);
		}
	}
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	public void setVisibility(boolean visible){
		this.frame.setVisible(visible);
	}
	
}
