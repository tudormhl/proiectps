package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Admin;

public class AdminView extends Observable implements ActionListener{

	
	private JPanel contentPane;
	private JFrame frame;
	private Admin admin;
	private JButton btnOrder ;
	
	public AdminView(Admin admin) {
		this.admin = admin;
		this.frame = new JFrame();
		this.frame.setVisible(true);
		
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		contentPane.setLayout(null);
		
		btnOrder = new JButton("See Activitiy");
		btnOrder.setBounds(67, 65, 150, 23);
		
		contentPane.add(btnOrder);
		this.frame.setContentPane(contentPane);
		
		this.btnOrder.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if( e.getSource() == this.btnOrder){
			setChanged();
			this.admin.setOperation("seeWaiterActivity");
			notifyObservers(admin);
		}
		
	}
	
	public void setVisibility(boolean visible){
		this.frame.setVisible(visible);
	}

}
