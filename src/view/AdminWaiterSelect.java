package view;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Admin;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class AdminWaiterSelect extends Observable implements ActionListener {

	private JPanel contentPane;
	private JComboBox<String> comboBox;
	private JFrame frame;
	private Admin admin;
	private JLabel lblSelect;
	
	private JButton orderBtn;

	
	public AdminWaiterSelect(Admin admin) {
		this.admin = admin;
		this.frame = new JFrame();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		this.frame.setVisible(true);
		this.contentPane = new JPanel();
		this.frame.add(contentPane);
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		
		lblSelect = new JLabel("Select waiter");
		lblSelect.setBounds(77, 50, 156, 14);
		this.contentPane.add(lblSelect);
		
		orderBtn = new JButton("Select");
		orderBtn.setBounds(77, 200, 120, 20);
		contentPane.add(orderBtn);
		
		this.orderBtn.addActionListener(this);
		
		//this.frame.add(contentPane);
		this.frame.repaint();

	}

	public void fillComboBox(String[] string){
		this.comboBox = new JComboBox<String>(string);
		comboBox.setBounds(77, 120, 152, 20);
		this.contentPane.add(comboBox);
		this.frame.repaint();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if( e.getSource() == this.orderBtn){
			setChanged();

			String s = comboBox.getSelectedItem().toString();
			
			this.admin.setOperation("seeActivity");
			this.admin.setRequestedInfo(s);
			
			notifyObservers(admin);
			this.frame.setVisible(false);
		}
	}
	
	public void setVisibility(boolean visible){
		this.frame.setVisible(visible);
	}
}
