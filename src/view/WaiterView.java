package view;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.Waiter;

import javax.swing.JButton;

public class WaiterView extends Observable implements ActionListener {

	private JPanel contentPane;
	private JFrame frame;
	private Waiter waiter;
	private JButton btnOrder ;
	private JButton btnBack ;
	private JButton btnHandlePayment;
	private JTextField textField;

	public WaiterView(Waiter waiter) {
		this.waiter = waiter;
		this.frame = new JFrame();
		this.frame.setVisible(true);
		
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		contentPane.setLayout(null);
		
		btnOrder = new JButton("Order");
		btnOrder.setBounds(67, 65, 89, 23);
		
		contentPane.add(btnOrder);
		this.frame.setContentPane(contentPane);
		
		btnHandlePayment = new JButton("Handle payment");
		btnHandlePayment.setBounds(52, 130, 125, 23);
		contentPane.add(btnHandlePayment);
		
		btnBack = new JButton("Back");
		btnBack.setBounds(67, 195, 89, 23);
		contentPane.add(btnBack);
		
		textField = new JTextField();
		textField.setBounds(244, 131, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		this.btnOrder.addActionListener(this);
		this.btnBack.addActionListener(this);
		this.btnHandlePayment.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if( e.getSource() == this.btnOrder){
			setChanged();
			System.out.println("View");
			this.waiter.setOperation("showProducts");
			notifyObservers(waiter);
		}
		
		if( e.getSource() == this.btnHandlePayment){
			setChanged();
			this.waiter.setOperation("deleteOrder");
			this.waiter.setRequestedInfo(Integer.parseInt(textField.getText()));
			notifyObservers(waiter);
			this.frame.setVisible(false);
		}
		
		if( e.getSource() == this.btnBack){
			setChanged();
			this.waiter.setOperation("goBack");
			notifyObservers(waiter);
			this.frame.setVisible(false);
		}
		
	}
	
	public void setVisibility(boolean visible){
		this.frame.setVisible(visible);
	}
}
