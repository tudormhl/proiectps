package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import model.KitchenEmployee;

public class KitchenEmployeeView extends Observable implements ActionListener {

	private JPanel contentPane;
	private JButton btnSituation;
	private JFrame frame;
	private JButton btnTimeTable;
	private JButton btnAAssignGrade;
	
	
	
	private KitchenEmployee  teacher;
	
	
	public KitchenEmployeeView(KitchenEmployee teacher) {
		
		  this.teacher = teacher;
		  this.frame = new JFrame();
		  this.contentPane = new JPanel();
		  
		  this.frame.setBounds(100, 100, 645, 379);
		  this.frame.setBackground(Color.BLUE);
		  this.frame.setVisible(true);
		  this.frame.setResizable(false);

		  this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		  this.contentPane.setLayout(null);
		  
		  
		  this.btnTimeTable = new JButton("Timetable");
		  this.btnTimeTable.addActionListener(this);
		  this.btnTimeTable.setBounds(94, 220, 200, 23);
		  this.contentPane.add(this.btnTimeTable);
		  
		  this.btnSituation = new JButton("Student Situation");
		  this.btnSituation.addActionListener(this);
		  this.btnSituation.setBounds(298, 220, 200, 23);
		  this.contentPane.add(this.btnSituation);
		  
		  this.btnAAssignGrade = new JButton("Assign Grades");
		  this.btnAAssignGrade.addActionListener(this);
		  this.btnAAssignGrade.setBounds(94, 280, 200, 23);
		  this.contentPane.add( this.btnAAssignGrade);
				  
		  // add to frame
		  this.frame.add(this.contentPane);	
		  this.frame.validate();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if( e.getSource() == this.btnTimeTable){
			 setChanged();
			 teacher.setOperation("teacherTimetable.read");
			 notifyObservers(teacher);
		}
		if( e.getSource() == this.btnSituation){
			 setChanged();
			 teacher.setOperation("teacherStudentSituation.read");
			 notifyObservers(teacher);
		}
		if( e.getSource() == this.btnAAssignGrade){
			 setChanged();
			 teacher.setOperation("teacherAssignGrades.read");
			 notifyObservers(teacher);
		}
		
		
	}
	
	public void setVisibility(Boolean visible){
		this.frame.setVisible(visible);
	}
}
