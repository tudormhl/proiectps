package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Waiter;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class OrderView extends Observable implements ActionListener {

	private JPanel contentPane;
	private JComboBox<String> comboBox;
	private JFrame frame;
	private Waiter waiter;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField table;
	private JLabel lblDrink, lblFood, lblDesert, lblQuantity, lblQuantity1, lblQuantity2, lblTable;
	
	private JComboBox<String> comboBox_1;
	private JComboBox<String> comboBox_2;
	
	private JButton orderBtn;
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					OrderView frame = new OrderView();
//					frame.setVisibility(true);;
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public OrderView(Waiter waiter) {
		this.waiter = waiter;
		this.frame = new JFrame();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		this.frame.setVisible(true);
		this.contentPane = new JPanel();
		this.frame.add(contentPane);
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(200, 34, 86, 20);
		this.contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(200, 113, 86, 20);
		this.contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(200, 185, 86, 20);
		this.contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		lblDesert = new JLabel("Desert");
		lblDesert.setBounds(21, 155, 46, 14);
		this.contentPane.add(lblDesert);
		
		lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(200, 155, 46, 14);
		this.contentPane.add(lblQuantity);
		
		lblDrink = new JLabel("Drink");
		lblDrink.setBounds(21, 77, 46, 14);
		this.contentPane.add(lblDrink);
		
		lblQuantity1 = new JLabel("Quantity");
		lblQuantity1.setBounds(200, 77, 46, 14);
		this.contentPane.add(lblQuantity1);
		
		lblQuantity2 = new JLabel("Quantity");
		lblQuantity2.setBounds(200, 11, 46, 14);
		this.contentPane.add(lblQuantity2);
		
		lblFood = new JLabel("Food");
		lblFood.setBounds(21, 11, 46, 14);
		this.contentPane.add(lblFood);
		
		lblTable = new JLabel("Table");
		lblTable.setBounds(335, 35, 46, 14);
		this.contentPane.add(lblTable);
		
		table = new JTextField();
		table.setBounds(335, 60, 46, 20);
		this.contentPane.add(table);
		table.setColumns(10);
		
		orderBtn = new JButton("Order");
		orderBtn.setBounds(250, 220, 80, 20);
		contentPane.add(orderBtn);
		
		this.orderBtn.addActionListener(this);
		
		//this.frame.add(contentPane);
		this.frame.repaint();

	}

	public void fillComboBox(String[] string){
		this.comboBox = new JComboBox<String>(string);
		comboBox.setBounds(21, 23, 102, 20);
		this.contentPane.add(comboBox);
		
		this.frame.repaint();
	}
	
	public void fillComboBox1(String[] string){
		this.comboBox_1 = new JComboBox<String>(string);
		comboBox_1.setBounds(21, 113, 102, 20);
		this.contentPane.add(comboBox_1);
		
		this.frame.repaint();
	}
	
	public void fillComboBox2(String[] string){
		this.comboBox_2 = new JComboBox<String>(string);
		comboBox_2.setBounds(21, 185, 102, 20);
		this.contentPane.add(comboBox_2);
		
		this.frame.repaint();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if( e.getSource() == this.orderBtn){
			setChanged();
			
			ArrayList<Integer> cantitati = new ArrayList<Integer>();
			cantitati.add(Integer.parseInt(textField.getText()));
			cantitati.add(Integer.parseInt(textField_1.getText()));
			cantitati.add(Integer.parseInt(textField_2.getText()));
			cantitati.add(Integer.parseInt(table.getText()));
			
			ArrayList<String> strings = new ArrayList<>();
			strings.add(comboBox.getSelectedItem().toString());
			strings.add(comboBox_1.getSelectedItem().toString());
			strings.add(comboBox_2.getSelectedItem().toString());
			
			System.out.println("View");
			
			this.waiter.setOperation("makeOrder");
			this.waiter.setRequestedInfo(cantitati);
			this.waiter.setRequestedInfo1(strings);
			
			notifyObservers(waiter);
			
			this.frame.setVisible(false);
		}
	}
	
	public void setVisibility(boolean visible){
		this.frame.setVisible(visible);
	}
}
