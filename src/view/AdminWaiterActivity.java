package view;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import model.Admin;
import model.Order;

import javax.swing.JButton;

public class AdminWaiterActivity extends Observable implements ActionListener {

	private JPanel contentPane;
	private JTable comboBox;
	private JFrame frame;
	private Admin admin;
	
	private JButton orderBtn;

	
	public AdminWaiterActivity(Admin admin) {
		this.admin = admin;
		this.frame = new JFrame();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 450, 300);
		this.frame.setVisible(true);
		this.contentPane = new JPanel();
		this.frame.add(contentPane);
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		
		orderBtn = new JButton("Back");
		orderBtn.setBounds(77, 230, 120, 20);
		contentPane.add(orderBtn);
		
		this.orderBtn.addActionListener(this);
		
		this.frame.repaint();

	}

	public void fillTable(List<Order> oder){
		this.comboBox = new JTable();
		comboBox.setBounds(77, 18, 500, 190);
		this.contentPane.add(comboBox);
		this.frame.repaint();
		
		DefaultTableModel dtm = new DefaultTableModel();
		dtm.addColumn("id");
		dtm.addColumn("tableNr");
		dtm.addColumn("idProdus");
		dtm.addColumn("idDrink");
		dtm.addColumn("idDesert");
		
		this.comboBox.setModel(dtm);
		this.comboBox.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 250), 1, true));
		this.comboBox.setFont(new java.awt.Font("Arial", 0, 18));
		dtm.addRow(new Object[] { "Activity :" });
		dtm.addRow(new Object[] {""});
		String[] colomnTitle = {"Id","Table","Id Produs","Id Drink", "Id Desert"};
		dtm.addRow(colomnTitle);
		
		for(Order key: oder) {
			   String[] line = {key.getId()+"",key.getTableNr()+"",key.getProductId()+"", key.getDrinkId()+"",key.getDesertId()+""  };
				dtm.addRow(line);
		}
		
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if( e.getSource() == this.orderBtn){
			setChanged();
			
			this.admin.setOperation("back");
			
			notifyObservers(admin);
			this.frame.setVisible(false);
		}
	}
	
	public void setVisibility(boolean visible){
		this.frame.setVisible(visible);
	}
}
