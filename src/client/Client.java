package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

import model.Admin;
import model.KitchenEmployee;
import model.Order;
import model.Waiter;
import view.WaiterView;
import view.AdminView;
import view.AdminWaiterActivity;
import view.AdminWaiterSelect;
import view.KitchenEmployeeView;
import view.LoginView;
import view.OrderView;

public class Client implements Runnable {

	private Socket clientSocket;
	
	private ObjectOutputStream output;
	private ObjectInputStream input;

	private Thread clientReader;

	private String host;
	private int port;
	
	private WaiterView waiterView;
	private OrderView orderView;
	private LoginView loginView;
	
	private AdminView av;
	private AdminWaiterSelect aws;
	private AdminWaiterActivity awa;
	
	private KitchenEmployeeView kitchenEmployeeView;
	
	public Client(String host, int port){
		this.host = host;
	 	this.port = port;
	}
	
	final public void openConnection() {
		if( isConnected() )
			return;
		try{
			this.clientSocket= new Socket(this.host, this.port);
			this.output = new ObjectOutputStream(this.clientSocket.getOutputStream());
			this.input = new ObjectInputStream(this.clientSocket.getInputStream());
		}catch (IOException ex){
			try{
				closeAll();
			}catch (Exception e) { 
				e.printStackTrace();
			}
		}
		this.clientReader = new Thread(this);  //Create the data reader thread
		this.clientReader.start();  //Start the thread
		}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Object msg;
		
		while(true){
			try{
				msg = input.readObject();
				handleMessageFromServer(msg);
			}catch(Exception e){
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}
	
	final public boolean isConnected(){
		return this.clientReader != null && this.clientReader.isAlive();
	}
	
	private void closeAll() {
		this.output = null;
		this.input = null;
		this.clientSocket = null;
	}
	
	public void handleMessageFromServer(Object msg) {
		if ( msg instanceof Waiter){
			if( ((Waiter) msg).getOperation().equals("loginSuccesWaiter") ){
				System.out.print("djkshdajkhdjka");
				this.waiterView = new WaiterView((Waiter) msg);
				this.waiterView.addObserver(ClientMain.getControllerClient());
			}
			
			if ( msg instanceof Waiter ){ 
				if (((Waiter) msg).getOperation().equals("showProducts")){
							@SuppressWarnings("unchecked")							
							List<String> map = (List<String>)((Waiter) msg).getRequestedInfo();
							@SuppressWarnings("unchecked")
							List<String> map1 = (List<String>)((Waiter) msg).getRequestedInfo1();
							@SuppressWarnings("unchecked")
							List<String> map2 = (List<String>)((Waiter) msg).getRequestedInfo2();
							this.waiterView.setVisibility(false);
							this.orderView = new OrderView((Waiter) msg);
							this.orderView.addObserver(ClientMain.getControllerClient());
							String[] stringArray =map.toArray(new String[0]); 
							String[] stringArray1 =map1.toArray(new String[0]); 
							String[] stringArray2 =map2.toArray(new String[0]); 
							this.orderView.fillComboBox(stringArray);		
							this.orderView.fillComboBox1(stringArray1);
							this.orderView.fillComboBox2(stringArray2);
				}
		}
			if( ((Waiter) msg).getOperation().equals("succesInsertOrder") ){
				System.out.print("o intrat comanda");
				this.waiterView = new WaiterView((Waiter) msg);
				this.waiterView.addObserver(ClientMain.getControllerClient());
			}
			
			if( ((Waiter) msg).getOperation().equals("succesDeleteOrder") ){
				System.out.print("o iesit comanda");
				this.waiterView = new WaiterView((Waiter) msg);
				this.waiterView.addObserver(ClientMain.getControllerClient());
			}
			
			if( ((Waiter) msg).getOperation().equals("succesGoBack") ){
				System.out.print("o mers inapoi");
				this.loginView = new LoginView();
				loginView.setVisibility(true);
				this.loginView.addObserver(ClientMain.getControllerClient());
			}
			
		}
		if ( msg instanceof KitchenEmployee){
			if( ((KitchenEmployee) msg).getOperation().equals("loginSuccesKitchenEmployee") ){
				this.kitchenEmployeeView = new KitchenEmployeeView(((KitchenEmployee) msg));
				this.kitchenEmployeeView.addObserver(ClientMain.getControllerClient());
			}
		}
		
		if ( msg instanceof Admin){
			if( ((Admin) msg).getOperation().equals("loginSuccesAdmin") ){
				this.av = new AdminView(((Admin) msg));
				this.av.addObserver(ClientMain.getControllerClient());
			}
			if (((Admin) msg).getOperation().equals("seeWaiterActivity")){	
				@SuppressWarnings("unchecked")
				List<String> waiters = (List<String>)((Admin) msg).getRequestedInfo();
				String[] strarray = waiters.toArray(new String[0]);
				this.aws = new AdminWaiterSelect((Admin) msg);
				this.aws.addObserver(ClientMain.getControllerClient());
				this.aws.fillComboBox(strarray);
			}
			if (((Admin) msg).getOperation().equals("seeActivity")){	
				@SuppressWarnings("unchecked")
				List<Order> order  = (List<Order>)((Admin) msg).getRequestedInfo();
				this.aws.setVisibility(false);
				this.awa = new AdminWaiterActivity((Admin) msg);
				this.awa.addObserver(ClientMain.getControllerClient());
				this.awa.fillTable(order);
				
			}
			
		}
		
		
	}
	
	public void sendToServer(Object msg){
		if (clientSocket == null || output == null) {
			System.out.println("socket does not exist");
		}	
		try {
			output.writeObject(msg);
		} catch (IOException e) {	
			e.printStackTrace();
		}
	}
	
	
}
