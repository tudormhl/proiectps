package client;

import controller.ClientController;
import view.LoginView;

public class ClientMain {

	private static ClientController controllerClient;
	
	public static void main(String [] args){
		String host = "localhost";
		int port = 1293;
		
		Client client=new Client(host,port);
		client.openConnection();
		
		LoginView login = new LoginView();
		login.getFrame().setVisible(true);
		
		controllerClient = new ClientController(client);
		login.addObserver(controllerClient);
		
		client.sendToServer("createControllerLogin");
	}

	public static ClientController getControllerClient() {
		return controllerClient;
	}

	public static void setControllerClient(ClientController controllerClient) {
		ClientMain.controllerClient = controllerClient;
	}
}
